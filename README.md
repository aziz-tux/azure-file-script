# README

## Usage

1. Install required dependencies
   `pip3 install -r requirements.txt`

2. Copy `sample.env` to `.env` and change the values.

3. Run script
   `python3 script.py`

### Script list

`azure_file_list.py ` - List all files on remote server.

`azure_file_put.py` - Upload file to remote server.

`azure_down.py` - Download files or directory.

`azure_file_del.py` - Delete file on remote server.
