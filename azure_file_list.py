#!/usr/bin/env python3

import os
from dotenv import load_dotenv
from azure.storage.fileshare import ShareServiceClient

# Load environment variables from .env file
load_dotenv()

# Read the details from the environment variables
account_name = os.getenv('ACCOUNT_NAME')
account_key = os.getenv('ACCOUNT_KEY')
share_name = os.getenv('SHARE_NAME')
directory_name = os.getenv('DIRECTORY_NAME')

# Create a ShareServiceClient using the account name and key
service_client = ShareServiceClient(account_url=f"https://{account_name}.file.core.windows.net/", credential=account_key)

# Get the share client
share_client = service_client.get_share_client(share_name)

# Get a directory client (root directory if not specified)
directory_client = share_client.get_directory_client(directory_name)

# Example: List files in the directory
print("Files in the directory:")
for item in directory_client.list_directories_and_files():
    print(item.name)