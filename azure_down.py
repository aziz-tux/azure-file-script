#!/usr/bin/env python3

import os
from dotenv import load_dotenv
from azure.storage.fileshare import ShareServiceClient

# Load environment variables from .env file
load_dotenv()

# Read the details from the environment variables
account_name = os.getenv('ACCOUNT_NAME')
account_key = os.getenv('ACCOUNT_KEY')
share_name = os.getenv('SHARE_NAME')
path_name = os.getenv('PATH_NAME')
download_path = os.getenv('DOWNLOAD_PATH')

# Create a ShareServiceClient using the account name and key
service_client = ShareServiceClient(account_url=f"https://{account_name}.file.core.windows.net/", credential=account_key)

# Get the share client
share_client = service_client.get_share_client(share_name)

# Ensure the local directory exists
os.makedirs(download_path, exist_ok=True)

def download_directory(directory_client, local_path):
    for item in directory_client.list_directories_and_files():
        item_path = os.path.join(local_path, item.name)
        if item.is_directory:
            os.makedirs(item_path, exist_ok=True)
            subdir_client = directory_client.get_subdirectory_client(item.name)
            download_directory(subdir_client, item_path)
        else:
            file_client = directory_client.get_file_client(item.name)
            with open(item_path, "wb") as download_file:
                download_stream = file_client.download_file()
                download_file.write(download_stream.readall())
            print(f"File '{item.name}' downloaded to '{item_path}'")

# Get a directory client for the specified path (root directory if not specified)
directory_client = share_client.get_directory_client('')

def path_exists(directory_client, path_name):
    try:
        file_client = directory_client.get_file_client(path_name)
        file_client.get_file_properties()
        return 'file'
    except:
        try:
            subdir_client = directory_client.get_subdirectory_client(path_name)
            subdir_client.get_directory_properties()
            return 'directory'
        except:
            return None

# Determine if the path is a file or directory
path_type = path_exists(directory_client, path_name)

if path_type == 'file':
    # Download the file
    file_client = directory_client.get_file_client(path_name)
    local_file_path = os.path.join(download_path, os.path.basename(path_name))
    with open(local_file_path, "wb") as download_file:
        download_stream = file_client.download_file()
        download_file.write(download_stream.readall())
    print(f"File '{path_name}' downloaded to '{local_file_path}'")
elif path_type == 'directory':
    # Download the directory
    subdir_client = directory_client.get_subdirectory_client(path_name)
    local_dir_path = os.path.join(download_path, path_name)
    os.makedirs(local_dir_path, exist_ok=True)
    download_directory(subdir_client, local_dir_path)
    print(f"Directory '{path_name}' downloaded to '{local_dir_path}'")
else:
    print(f"The path '{path_name}' does not exist in the Azure File Share '{share_name}'")