#!/usr/bin/env python3

import os
from dotenv import load_dotenv
from azure.storage.fileshare import ShareServiceClient

# Load environment variables from .env file
load_dotenv()

# Define your credentials and share details
account_name = os.getenv('ACCOUNT_NAME')
account_key = os.getenv('ACCOUNT_KEY')
share_name = os.getenv('SHARE_NAME')
directory_name = os.getenv('DIRECTORY_NAME')
file_name = os.getenv('REMOTE_FILE_NAME')
local_file_path = os.getenv('LOCAL_FILE_NAME')

# Create a ShareServiceClient using the account name and key
service_client = ShareServiceClient(account_url=f"https://{account_name}.file.core.windows.net/", credential=account_key)

# Get the share client
share_client = service_client.get_share_client(share_name)

# Get a directory client (root directory if not specified)
directory_client = share_client.get_directory_client(directory_name)

# Get the file client
file_client = directory_client.get_file_client(file_name)

# Upload the local file to Azure File Share
with open(local_file_path, "rb") as source_file:
    file_client.upload_file(source_file)

print(f"File '{local_file_path}' uploaded to '{share_name}/{directory_name}/{file_name}' successfully.")